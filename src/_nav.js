import { menu } from "@/views/staticData";

const makeRequest = async () => {
  try {
    const res = await fetch('https://pokeapi.co/api/v2/pokemon/ditto');
    const data = await res.json();
    const result = JSON.stringify(data);
    return result;
  } catch (err) {
    console.error(err.message);
    return false;
  }
};

export default async function () {
  await makeRequest();
  
  // const menuArr = [
  //   // {
  //   //   component: 'CNavItem',
  //   //   name: 'Dashboard',
  //   //   to: '/dashboard',
  //   //   icon: 'cil-speedometer',
  //   // },
  //   {
  //     component: 'CNavItem',
  //     name: 'Futbol',
  //     icon: 'cicSoccer', // string -> 'cicSoccer' OR string[] -> ['width height', 'inner svg tags']
  //     // customClasses: [''],
  //     items: [
  //       {
  //         component: 'CNavItem',
  //         name: 'Champions',
  //         emiter: 'champions',
  //         to: '/',
  //       },
  //       {
  //         component: 'CNavItem',
  //         name: 'Premier League',
  //         emiter: 'premierleague',
  //         to: '/',
  //       },
  //     ],
  //   },
  //   {
  //     component: 'CNavItem',
  //     name: 'Beisbol',
  //     icon: 'cicBaseball',
  //     items: [
  //       {
  //         component: 'CNavItem',
  //         name: 'Major League Baseball',
  //         emiter: 'mlb',
  //         to: '/',
  //       },
  //     ],
  //   },
  // ];

  const menuArr = menu().map(menuItem => ({
    component: 'CNavItem',
    name: menuItem.desSport,
    icon: menuItem.icon, // string -> 'cicSoccer' OR string[] -> ['width height', 'inner svg tags']
    // customClasses: [''],
    items: menuItem.leagues.map(league => ({
      component: 'CNavItem',
      name: league.desLeague,
      emiter: league.idLeague,
      to: 'none',
    })),
  }));

  return menuArr;
}