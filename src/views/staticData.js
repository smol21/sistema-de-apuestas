const statictData = {
    data: {
      configSport: {
        baseAchievement: 100,
        betUnit: 1,
        desMessage: null,
        estAnnulled: "1",
        idOperator: "TzAxNg==",
        maxFemale: "5",
        minimunBet: 5,//9000,
        multiplyingFactor: 1,
        staMessage: "0",
        staPointAuto: "1",
        staSport: "1",
        staTicketOffice: "0",
        ticketExpiration: "5",
        //  Invented for testing
        maxBets: "10",
      },
      lstDSport: 
        {
          desSport: "FUTBOL",
          idSport: "MDI=",
          staParlay: "1"
        }
      ,
      lstDtBMove: [
        {
          amoProfit: 250000000,
          desMove: "DERECHA",
          idMove: "MDE="
        },
        {
          amoProfit: 250000000,
          desMove: "PARLAY",
          idMove: "MDI="
        }
      ],
      lstDtBet: [
        {
          desBet: "GANADOR",
          idBet: "MDE=",
          prepositionBet: "0",
          typeElement: "3"
        },
        {
          desBet: "RUNLINE",
          idBet: "MDI=",
          prepositionBet: "0",
          typeElement: "1"
        },
        {
          desBet: "TOTALES",
          idBet: "MDM=",
          prepositionBet: "0",
          typeElement: "2"
        }
      ],
      lstDtBetAllow: [
        {
          idSport: "MDI=",
          lstBet: [
            {
              idBet: "MDE=",
              lstBetTyp: [
                "MDE="
              ]
            }
          ]
        }
      ],
      lstDtEvent: [
        {
          dateClosing: "1608655500",
          dateEvent: "1608656100",
          dateReprogramming: "1608656100",
          desEvent: "NAPOLI        VS AZ ALKMAAR   ",
          idEvent: "Nzc2ODU=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "-550",
              conditionAchievement: null,
              desElement: "NAPOLI",
              idAchievement: "MDE0Mg==",
              idBet: "MDE=",
              idElement: "WDAxNDU=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15964"
            },
            {
              achievementValue: "+1100",
              conditionAchievement: null,
              desElement: "AZ ALKMAAR",
              idAchievement: "MDE0Mw==",
              idBet: "MDE=",
              idElement: "WDAyMzM=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15965"
            },
            {
              achievementValue: "+675",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE0NA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15966"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-2",
              desElement: "NAPOLI",
              idAchievement: "MDE0NQ==",
              idBet: "MDI=",
              idElement: "WDAxNDU=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15964"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+2",
              desElement: "AZ ALKMAAR",
              idAchievement: "MDE0Ng==",
              idBet: "MDI=",
              idElement: "WDAyMzM=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15965"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "3,5",
              desElement: "ALTA",
              idAchievement: "MDE0Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODU=",
              officialRotation: "0"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "3,5",
              desElement: "BAJA",
              idAchievement: "MDE0OA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODU=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608655500",
          dateEvent: "1608656100",
          dateReprogramming: "1608656100",
          desEvent: "NAPOLI        VS AZ ALKMAAR   ",
          idEvent: "Nzc2ODU=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "-550",
              conditionAchievement: null,
              desElement: "NAPOLI",
              idAchievement: "MDE0Mg==",
              idBet: "MDE=",
              idElement: "WDAxNDU=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15964"
            },
            {
              achievementValue: "+1100",
              conditionAchievement: null,
              desElement: "AZ ALKMAAR",
              idAchievement: "MDE0Mw==",
              idBet: "MDE=",
              idElement: "WDAyMzM=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15965"
            },
            {
              achievementValue: "+675",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE0NA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15966"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-2",
              desElement: "NAPOLI",
              idAchievement: "MDE0NQ==",
              idBet: "MDI=",
              idElement: "WDAxNDU=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15964"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+2",
              desElement: "AZ ALKMAAR",
              idAchievement: "MDE0Ng==",
              idBet: "MDI=",
              idElement: "WDAyMzM=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15965"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "3,5",
              desElement: "ALTA",
              idAchievement: "MDE0Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODU=",
              officialRotation: "0"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "3,5",
              desElement: "BAJA",
              idAchievement: "MDE0OA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODU=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608655500",
          dateEvent: "1608656100",
          dateReprogramming: "1608656100",
          desEvent: "NAPOLI        VS AZ ALKMAAR   ",
          idEvent: "Nzc2ODU=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "-550",
              conditionAchievement: null,
              desElement: "NAPOLI",
              idAchievement: "MDE0Mg==",
              idBet: "MDE=",
              idElement: "WDAxNDU=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15964"
            },
            {
              achievementValue: "+1100",
              conditionAchievement: null,
              desElement: "AZ ALKMAAR",
              idAchievement: "MDE0Mw==",
              idBet: "MDE=",
              idElement: "WDAyMzM=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15965"
            },
            {
              achievementValue: "+675",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE0NA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15966"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-2",
              desElement: "NAPOLI",
              idAchievement: "MDE0NQ==",
              idBet: "MDI=",
              idElement: "WDAxNDU=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15964"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+2",
              desElement: "AZ ALKMAAR",
              idAchievement: "MDE0Ng==",
              idBet: "MDI=",
              idElement: "WDAyMzM=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15965"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "3,5",
              desElement: "ALTA",
              idAchievement: "MDE0Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODU=",
              officialRotation: "0"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "3,5",
              desElement: "BAJA",
              idAchievement: "MDE0OA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODU=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608655500",
          dateEvent: "1608656100",
          dateReprogramming: "1608656100",
          desEvent: "RAPID VIENNA  VS ARSENAL      ",
          idEvent: "Nzc2ODc=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+435",
              conditionAchievement: null,
              desElement: "RAPID VIENNA",
              idAchievement: "MDE1Ng==",
              idBet: "MDE=",
              idElement: "WDAzNTA=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15970"
            },
            {
              achievementValue: "-170",
              conditionAchievement: null,
              desElement: "ARSENAL",
              idAchievement: "MDE1Nw==",
              idBet: "MDE=",
              idElement: "WDAxODE=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15971"
            },
            {
              achievementValue: "+300",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE1OA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15972"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+1",
              desElement: "RAPID VIENNA",
              idAchievement: "MDE1OQ==",
              idBet: "MDI=",
              idElement: "WDAzNTA=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15970"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-1",
              desElement: "ARSENAL",
              idAchievement: "MDE2MA==",
              idBet: "MDI=",
              idElement: "WDAxODE=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15971"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "ALTA",
              idAchievement: "MDE2MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODc=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "BAJA",
              idAchievement: "MDE2Mg==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODc=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608655500",
          dateEvent: "1608656100",
          dateReprogramming: "1608656100",
          desEvent: "RAPID VIENNA  VS ARSENAL      ",
          idEvent: "Nzc2ODc=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+435",
              conditionAchievement: null,
              desElement: "RAPID VIENNA",
              idAchievement: "MDE1Ng==",
              idBet: "MDE=",
              idElement: "WDAzNTA=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15970"
            },
            {
              achievementValue: "-170",
              conditionAchievement: null,
              desElement: "ARSENAL",
              idAchievement: "MDE1Nw==",
              idBet: "MDE=",
              idElement: "WDAxODE=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15971"
            },
            {
              achievementValue: "+300",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE1OA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15972"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+1",
              desElement: "RAPID VIENNA",
              idAchievement: "MDE1OQ==",
              idBet: "MDI=",
              idElement: "WDAzNTA=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15970"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-1",
              desElement: "ARSENAL",
              idAchievement: "MDE2MA==",
              idBet: "MDI=",
              idElement: "WDAxODE=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15971"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "ALTA",
              idAchievement: "MDE2MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODc=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "BAJA",
              idAchievement: "MDE2Mg==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODc=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608655500",
          dateEvent: "1608656100",
          dateReprogramming: "1608656100",
          desEvent: "RAPID VIENNA  VS ARSENAL      ",
          idEvent: "Nzc2ODc=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+435",
              conditionAchievement: null,
              desElement: "RAPID VIENNA",
              idAchievement: "MDE1Ng==",
              idBet: "MDE=",
              idElement: "WDAzNTA=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15970"
            },
            {
              achievementValue: "-170",
              conditionAchievement: null,
              desElement: "ARSENAL",
              idAchievement: "MDE1Nw==",
              idBet: "MDE=",
              idElement: "WDAxODE=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15971"
            },
            {
              achievementValue: "+300",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE1OA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15972"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+1",
              desElement: "RAPID VIENNA",
              idAchievement: "MDE1OQ==",
              idBet: "MDI=",
              idElement: "WDAzNTA=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15970"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-1",
              desElement: "ARSENAL",
              idAchievement: "MDE2MA==",
              idBet: "MDI=",
              idElement: "WDAxODE=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15971"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "ALTA",
              idAchievement: "MDE2MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODc=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "BAJA",
              idAchievement: "MDE2Mg==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODc=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608655500",
          dateEvent: "1608656100",
          dateReprogramming: "1608656100",
          desEvent: "ST. LIEGE     VS RANGERS      ",
          idEvent: "Nzc2ODk=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+200",
              conditionAchievement: null,
              desElement: "ST. LIEGE",
              idAchievement: "MDE3MA==",
              idBet: "MDE=",
              idElement: "WDEyMTQ=",
              idEvent: "Nzc2ODk=",
              officialRotation: "15976"
            },
            {
              achievementValue: "+125",
              conditionAchievement: null,
              desElement: "RANGERS",
              idAchievement: "MDE3MQ==",
              idBet: "MDE=",
              idElement: "WDE3NzM=",
              idEvent: "Nzc2ODk=",
              officialRotation: "15977"
            },
            {
              achievementValue: "+260",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE3Mg==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODk=",
              officialRotation: "15978"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDE3Mw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODk=",
              officialRotation: "0"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDE3NA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODk=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODk=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODk=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608655500",
          dateEvent: "1608656100",
          dateReprogramming: "1608656100",
          desEvent: "ST. LIEGE     VS RANGERS      ",
          idEvent: "Nzc2ODk=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+200",
              conditionAchievement: null,
              desElement: "ST. LIEGE",
              idAchievement: "MDE3MA==",
              idBet: "MDE=",
              idElement: "WDEyMTQ=",
              idEvent: "Nzc2ODk=",
              officialRotation: "15976"
            },
            {
              achievementValue: "+125",
              conditionAchievement: null,
              desElement: "RANGERS",
              idAchievement: "MDE3MQ==",
              idBet: "MDE=",
              idElement: "WDE3NzM=",
              idEvent: "Nzc2ODk=",
              officialRotation: "15977"
            },
            {
              achievementValue: "+260",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE3Mg==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODk=",
              officialRotation: "15978"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDE3Mw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODk=",
              officialRotation: "0"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDE3NA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODk=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODk=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODk=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608655500",
          dateEvent: "1608656100",
          dateReprogramming: "1608656100",
          desEvent: "ST. LIEGE     VS RANGERS      ",
          idEvent: "Nzc2ODk=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+200",
              conditionAchievement: null,
              desElement: "ST. LIEGE",
              idAchievement: "MDE3MA==",
              idBet: "MDE=",
              idElement: "WDEyMTQ=",
              idEvent: "Nzc2ODk=",
              officialRotation: "15976"
            },
            {
              achievementValue: "+125",
              conditionAchievement: null,
              desElement: "RANGERS",
              idAchievement: "MDE3MQ==",
              idBet: "MDE=",
              idElement: "WDE3NzM=",
              idEvent: "Nzc2ODk=",
              officialRotation: "15977"
            },
            {
              achievementValue: "+260",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE3Mg==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODk=",
              officialRotation: "15978"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDE3Mw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODk=",
              officialRotation: "0"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDE3NA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODk=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODk=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODk=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608655500",
          dateEvent: "1608656100",
          dateReprogramming: "1608656100",
          desEvent: "NAPOLI        VS AZ ALKMAAR   ",
          idEvent: "Nzc2ODU=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "-550",
              conditionAchievement: null,
              desElement: "NAPOLI",
              idAchievement: "MDE0Mg==",
              idBet: "MDE=",
              idElement: "WDAxNDU=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15964"
            },
            {
              achievementValue: "+1100",
              conditionAchievement: null,
              desElement: "AZ ALKMAAR",
              idAchievement: "MDE0Mw==",
              idBet: "MDE=",
              idElement: "WDAyMzM=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15965"
            },
            {
              achievementValue: "+675",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE0NA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15966"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-2",
              desElement: "NAPOLI",
              idAchievement: "MDE0NQ==",
              idBet: "MDI=",
              idElement: "WDAxNDU=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15964"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+2",
              desElement: "AZ ALKMAAR",
              idAchievement: "MDE0Ng==",
              idBet: "MDI=",
              idElement: "WDAyMzM=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15965"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "3,5",
              desElement: "ALTA",
              idAchievement: "MDE0Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODU=",
              officialRotation: "0"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "3,5",
              desElement: "BAJA",
              idAchievement: "MDE0OA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODU=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608655500",
          dateEvent: "1608656100",
          dateReprogramming: "1608656100",
          desEvent: "NAPOLI        VS AZ ALKMAAR   ",
          idEvent: "Nzc2ODU=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "-550",
              conditionAchievement: null,
              desElement: "NAPOLI",
              idAchievement: "MDE0Mg==",
              idBet: "MDE=",
              idElement: "WDAxNDU=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15964"
            },
            {
              achievementValue: "+1100",
              conditionAchievement: null,
              desElement: "AZ ALKMAAR",
              idAchievement: "MDE0Mw==",
              idBet: "MDE=",
              idElement: "WDAyMzM=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15965"
            },
            {
              achievementValue: "+675",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE0NA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15966"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-2",
              desElement: "NAPOLI",
              idAchievement: "MDE0NQ==",
              idBet: "MDI=",
              idElement: "WDAxNDU=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15964"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+2",
              desElement: "AZ ALKMAAR",
              idAchievement: "MDE0Ng==",
              idBet: "MDI=",
              idElement: "WDAyMzM=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15965"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "3,5",
              desElement: "ALTA",
              idAchievement: "MDE0Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODU=",
              officialRotation: "0"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "3,5",
              desElement: "BAJA",
              idAchievement: "MDE0OA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODU=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608655500",
          dateEvent: "1608656100",
          dateReprogramming: "1608656100",
          desEvent: "RAPID VIENNA  VS ARSENAL      ",
          idEvent: "Nzc2ODc=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+435",
              conditionAchievement: null,
              desElement: "RAPID VIENNA",
              idAchievement: "MDE1Ng==",
              idBet: "MDE=",
              idElement: "WDAzNTA=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15970"
            },
            {
              achievementValue: "-170",
              conditionAchievement: null,
              desElement: "ARSENAL",
              idAchievement: "MDE1Nw==",
              idBet: "MDE=",
              idElement: "WDAxODE=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15971"
            },
            {
              achievementValue: "+300",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE1OA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15972"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+1",
              desElement: "RAPID VIENNA",
              idAchievement: "MDE1OQ==",
              idBet: "MDI=",
              idElement: "WDAzNTA=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15970"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-1",
              desElement: "ARSENAL",
              idAchievement: "MDE2MA==",
              idBet: "MDI=",
              idElement: "WDAxODE=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15971"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "ALTA",
              idAchievement: "MDE2MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODc=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "BAJA",
              idAchievement: "MDE2Mg==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODc=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608655500",
          dateEvent: "1608656100",
          dateReprogramming: "1608656100",
          desEvent: "RAPID VIENNA  VS ARSENAL      ",
          idEvent: "Nzc2ODc=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+435",
              conditionAchievement: null,
              desElement: "RAPID VIENNA",
              idAchievement: "MDE1Ng==",
              idBet: "MDE=",
              idElement: "WDAzNTA=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15970"
            },
            {
              achievementValue: "-170",
              conditionAchievement: null,
              desElement: "ARSENAL",
              idAchievement: "MDE1Nw==",
              idBet: "MDE=",
              idElement: "WDAxODE=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15971"
            },
            {
              achievementValue: "+300",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE1OA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15972"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+1",
              desElement: "RAPID VIENNA",
              idAchievement: "MDE1OQ==",
              idBet: "MDI=",
              idElement: "WDAzNTA=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15970"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-1",
              desElement: "ARSENAL",
              idAchievement: "MDE2MA==",
              idBet: "MDI=",
              idElement: "WDAxODE=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15971"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "ALTA",
              idAchievement: "MDE2MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODc=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "BAJA",
              idAchievement: "MDE2Mg==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODc=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608655500",
          dateEvent: "1608656100",
          dateReprogramming: "1608656100",
          desEvent: "RAPID VIENNA  VS ARSENAL      ",
          idEvent: "Nzc2ODc=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+435",
              conditionAchievement: null,
              desElement: "RAPID VIENNA",
              idAchievement: "MDE1Ng==",
              idBet: "MDE=",
              idElement: "WDAzNTA=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15970"
            },
            {
              achievementValue: "-170",
              conditionAchievement: null,
              desElement: "ARSENAL",
              idAchievement: "MDE1Nw==",
              idBet: "MDE=",
              idElement: "WDAxODE=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15971"
            },
            {
              achievementValue: "+300",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE1OA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15972"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+1",
              desElement: "RAPID VIENNA",
              idAchievement: "MDE1OQ==",
              idBet: "MDI=",
              idElement: "WDAzNTA=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15970"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-1",
              desElement: "ARSENAL",
              idAchievement: "MDE2MA==",
              idBet: "MDI=",
              idElement: "WDAxODE=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15971"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "ALTA",
              idAchievement: "MDE2MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODc=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "BAJA",
              idAchievement: "MDE2Mg==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODc=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608655500",
          dateEvent: "1608656100",
          dateReprogramming: "1608656100",
          desEvent: "NAPOLI        VS AZ ALKMAAR   ",
          idEvent: "Nzc2ODU=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "-550",
              conditionAchievement: null,
              desElement: "NAPOLI",
              idAchievement: "MDE0Mg==",
              idBet: "MDE=",
              idElement: "WDAxNDU=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15964"
            },
            {
              achievementValue: "+1100",
              conditionAchievement: null,
              desElement: "AZ ALKMAAR",
              idAchievement: "MDE0Mw==",
              idBet: "MDE=",
              idElement: "WDAyMzM=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15965"
            },
            {
              achievementValue: "+675",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE0NA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15966"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-2",
              desElement: "NAPOLI",
              idAchievement: "MDE0NQ==",
              idBet: "MDI=",
              idElement: "WDAxNDU=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15964"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+2",
              desElement: "AZ ALKMAAR",
              idAchievement: "MDE0Ng==",
              idBet: "MDI=",
              idElement: "WDAyMzM=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15965"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "3,5",
              desElement: "ALTA",
              idAchievement: "MDE0Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODU=",
              officialRotation: "0"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "3,5",
              desElement: "BAJA",
              idAchievement: "MDE0OA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODU=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608655500",
          dateEvent: "1608656100",
          dateReprogramming: "1608656100",
          desEvent: "ST. LIEGE     VS RANGERS      ",
          idEvent: "Nzc2ODk=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+200",
              conditionAchievement: null,
              desElement: "ST. LIEGE",
              idAchievement: "MDE3MA==",
              idBet: "MDE=",
              idElement: "WDEyMTQ=",
              idEvent: "Nzc2ODk=",
              officialRotation: "15976"
            },
            {
              achievementValue: "+125",
              conditionAchievement: null,
              desElement: "RANGERS",
              idAchievement: "MDE3MQ==",
              idBet: "MDE=",
              idElement: "WDE3NzM=",
              idEvent: "Nzc2ODk=",
              officialRotation: "15977"
            },
            {
              achievementValue: "+260",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE3Mg==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODk=",
              officialRotation: "15978"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDE3Mw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODk=",
              officialRotation: "0"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDE3NA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODk=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODk=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODk=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608655500",
          dateEvent: "1608656100",
          dateReprogramming: "1608656100",
          desEvent: "RAPID VIENNA  VS ARSENAL      ",
          idEvent: "Nzc2ODc=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+435",
              conditionAchievement: null,
              desElement: "RAPID VIENNA",
              idAchievement: "MDE1Ng==",
              idBet: "MDE=",
              idElement: "WDAzNTA=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15970"
            },
            {
              achievementValue: "-170",
              conditionAchievement: null,
              desElement: "ARSENAL",
              idAchievement: "MDE1Nw==",
              idBet: "MDE=",
              idElement: "WDAxODE=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15971"
            },
            {
              achievementValue: "+300",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE1OA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15972"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+1",
              desElement: "RAPID VIENNA",
              idAchievement: "MDE1OQ==",
              idBet: "MDI=",
              idElement: "WDAzNTA=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15970"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-1",
              desElement: "ARSENAL",
              idAchievement: "MDE2MA==",
              idBet: "MDI=",
              idElement: "WDAxODE=",
              idEvent: "Nzc2ODc=",
              officialRotation: "15971"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "ALTA",
              idAchievement: "MDE2MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODc=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "BAJA",
              idAchievement: "MDE2Mg==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODc=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODc=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608655500",
          dateEvent: "1608656100",
          dateReprogramming: "1608656100",
          desEvent: "NAPOLI        VS AZ ALKMAAR   ",
          idEvent: "Nzc2ODU=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "-550",
              conditionAchievement: null,
              desElement: "NAPOLI",
              idAchievement: "MDE0Mg==",
              idBet: "MDE=",
              idElement: "WDAxNDU=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15964"
            },
            {
              achievementValue: "+1100",
              conditionAchievement: null,
              desElement: "AZ ALKMAAR",
              idAchievement: "MDE0Mw==",
              idBet: "MDE=",
              idElement: "WDAyMzM=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15965"
            },
            {
              achievementValue: "+675",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE0NA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15966"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-2",
              desElement: "NAPOLI",
              idAchievement: "MDE0NQ==",
              idBet: "MDI=",
              idElement: "WDAxNDU=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15964"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+2",
              desElement: "AZ ALKMAAR",
              idAchievement: "MDE0Ng==",
              idBet: "MDI=",
              idElement: "WDAyMzM=",
              idEvent: "Nzc2ODU=",
              officialRotation: "15965"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "3,5",
              desElement: "ALTA",
              idAchievement: "MDE0Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODU=",
              officialRotation: "0"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "3,5",
              desElement: "BAJA",
              idAchievement: "MDE0OA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODU=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODU=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608655500",
          dateEvent: "1608656100",
          dateReprogramming: "1608656100",
          desEvent: "ST. LIEGE     VS RANGERS      ",
          idEvent: "Nzc2ODk=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+200",
              conditionAchievement: null,
              desElement: "ST. LIEGE",
              idAchievement: "MDE3MA==",
              idBet: "MDE=",
              idElement: "WDEyMTQ=",
              idEvent: "Nzc2ODk=",
              officialRotation: "15976"
            },
            {
              achievementValue: "+125",
              conditionAchievement: null,
              desElement: "RANGERS",
              idAchievement: "MDE3MQ==",
              idBet: "MDE=",
              idElement: "WDE3NzM=",
              idEvent: "Nzc2ODk=",
              officialRotation: "15977"
            },
            {
              achievementValue: "+260",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE3Mg==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODk=",
              officialRotation: "15978"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDE3Mw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODk=",
              officialRotation: "0"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDE3NA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODk=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODk=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODk=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608669900",
          dateEvent: "1608670800",
          dateReprogramming: "1608674100",
          desEvent: "H BEER SHEVA  VS SLAVIA PRAGUE",
          idEvent: "Nzc4NzU=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+410",
              conditionAchievement: null,
              desElement: "H BEER SHEVA",
              idAchievement: "MDAwOA==",
              idBet: "MDE=",
              idElement: "WDExMTk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16037"
            },
            {
              achievementValue: "-160",
              conditionAchievement: null,
              desElement: "SLAVIA PRAGUE ",
              idAchievement: "MDAwOQ==",
              idBet: "MDE=",
              idElement: "WDExMjk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16038"
            },
            {
              achievementValue: "+310",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDAxMA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16039"
            },
            {
              achievementValue: "+120",
              conditionAchievement: "+0,5",
              desElement: "H BEER SHEVA",
              idAchievement: "MDAxMQ==",
              idBet: "MDI=",
              idElement: "WDExMTk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16037"
            },
            {
              achievementValue: "-160",
              conditionAchievement: "-0,5",
              desElement: "SLAVIA PRAGUE ",
              idAchievement: "MDAxMg==",
              idBet: "MDI=",
              idElement: "WDExMjk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16038"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDAxMw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4NzU=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDAxNA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4NzU=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608669900",
          dateEvent: "1608670800",
          dateReprogramming: "1608674100",
          desEvent: "H BEER SHEVA  VS SLAVIA PRAGUE",
          idEvent: "Nzc4NzU=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+410",
              conditionAchievement: null,
              desElement: "H BEER SHEVA",
              idAchievement: "MDAwOA==",
              idBet: "MDE=",
              idElement: "WDExMTk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16037"
            },
            {
              achievementValue: "-160",
              conditionAchievement: null,
              desElement: "SLAVIA PRAGUE ",
              idAchievement: "MDAwOQ==",
              idBet: "MDE=",
              idElement: "WDExMjk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16038"
            },
            {
              achievementValue: "+310",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDAxMA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16039"
            },
            {
              achievementValue: "+120",
              conditionAchievement: "+0,5",
              desElement: "H BEER SHEVA",
              idAchievement: "MDAxMQ==",
              idBet: "MDI=",
              idElement: "WDExMTk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16037"
            },
            {
              achievementValue: "-160",
              conditionAchievement: "-0,5",
              desElement: "SLAVIA PRAGUE ",
              idAchievement: "MDAxMg==",
              idBet: "MDI=",
              idElement: "WDExMjk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16038"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDAxMw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4NzU=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDAxNA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4NzU=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608669900",
          dateEvent: "1608670800",
          dateReprogramming: "1608674100",
          desEvent: "H BEER SHEVA  VS SLAVIA PRAGUE",
          idEvent: "Nzc4NzU=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+410",
              conditionAchievement: null,
              desElement: "H BEER SHEVA",
              idAchievement: "MDAwOA==",
              idBet: "MDE=",
              idElement: "WDExMTk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16037"
            },
            {
              achievementValue: "-160",
              conditionAchievement: null,
              desElement: "SLAVIA PRAGUE ",
              idAchievement: "MDAwOQ==",
              idBet: "MDE=",
              idElement: "WDExMjk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16038"
            },
            {
              achievementValue: "+310",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDAxMA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16039"
            },
            {
              achievementValue: "+120",
              conditionAchievement: "+0,5",
              desElement: "H BEER SHEVA",
              idAchievement: "MDAxMQ==",
              idBet: "MDI=",
              idElement: "WDExMTk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16037"
            },
            {
              achievementValue: "-160",
              conditionAchievement: "-0,5",
              desElement: "SLAVIA PRAGUE ",
              idAchievement: "MDAxMg==",
              idBet: "MDI=",
              idElement: "WDExMjk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16038"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDAxMw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4NzU=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDAxNA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4NzU=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608669900",
          dateEvent: "1608670800",
          dateReprogramming: "1608656100",
          desEvent: "DUNDALK       VS MOLDE        ",
          idEvent: "Nzc4NzQ=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+380",
              conditionAchievement: null,
              desElement: "DUNDALK",
              idAchievement: "MDAxNQ==",
              idBet: "MDE=",
              idElement: "WDExMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16040"
            },
            {
              achievementValue: "-180",
              conditionAchievement: null,
              desElement: "MOLDE",
              idAchievement: "MDAxNg==",
              idBet: "MDE=",
              idElement: "WDA5Njc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16041"
            },
            {
              achievementValue: "+335",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDAxNw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16042"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+1",
              desElement: "DUNDALK",
              idAchievement: "MDAxOA==",
              idBet: "MDI=",
              idElement: "WDExMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16040"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-1",
              desElement: "MOLDE",
              idAchievement: "MDAxOQ==",
              idBet: "MDI=",
              idElement: "WDA5Njc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16041"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "ALTA",
              idAchievement: "MDAyMA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "BAJA",
              idAchievement: "MDAyMQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608669900",
          dateEvent: "1608670800",
          dateReprogramming: "1608656100",
          desEvent: "DUNDALK       VS MOLDE        ",
          idEvent: "Nzc4NzQ=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+380",
              conditionAchievement: null,
              desElement: "DUNDALK",
              idAchievement: "MDAxNQ==",
              idBet: "MDE=",
              idElement: "WDExMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16040"
            },
            {
              achievementValue: "-180",
              conditionAchievement: null,
              desElement: "MOLDE",
              idAchievement: "MDAxNg==",
              idBet: "MDE=",
              idElement: "WDA5Njc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16041"
            },
            {
              achievementValue: "+335",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDAxNw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16042"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+1",
              desElement: "DUNDALK",
              idAchievement: "MDAxOA==",
              idBet: "MDI=",
              idElement: "WDExMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16040"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-1",
              desElement: "MOLDE",
              idAchievement: "MDAxOQ==",
              idBet: "MDI=",
              idElement: "WDA5Njc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16041"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "ALTA",
              idAchievement: "MDAyMA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "BAJA",
              idAchievement: "MDAyMQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608669900",
          dateEvent: "1608670800",
          dateReprogramming: "1608656100",
          desEvent: "DUNDALK       VS MOLDE        ",
          idEvent: "Nzc4NzQ=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+380",
              conditionAchievement: null,
              desElement: "DUNDALK",
              idAchievement: "MDAxNQ==",
              idBet: "MDE=",
              idElement: "WDExMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16040"
            },
            {
              achievementValue: "-180",
              conditionAchievement: null,
              desElement: "MOLDE",
              idAchievement: "MDAxNg==",
              idBet: "MDE=",
              idElement: "WDA5Njc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16041"
            },
            {
              achievementValue: "+335",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDAxNw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16042"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+1",
              desElement: "DUNDALK",
              idAchievement: "MDAxOA==",
              idBet: "MDI=",
              idElement: "WDExMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16040"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-1",
              desElement: "MOLDE",
              idAchievement: "MDAxOQ==",
              idBet: "MDI=",
              idElement: "WDA5Njc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16041"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "ALTA",
              idAchievement: "MDAyMA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "BAJA",
              idAchievement: "MDAyMQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "AC WOLFSBERGE VS CSKA MOSCOW  ",
          idEvent: "Nzc4ODA=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+225",
              conditionAchievement: null,
              desElement: "AC WOLFSBERGER",
              idAchievement: "MDA2Mg==",
              idBet: "MDE=",
              idElement: "WDE4MjU=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16043"
            },
            {
              achievementValue: "+105",
              conditionAchievement: null,
              desElement: "CSKA MOSCOW",
              idAchievement: "MDA2Mw==",
              idBet: "MDE=",
              idElement: "WDAzMzE=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16044"
            },
            {
              achievementValue: "+280",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA2NA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16045"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+0,5",
              desElement: "AC WOLFSBERGER",
              idAchievement: "MDA2NQ==",
              idBet: "MDI=",
              idElement: "WDE4MjU=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16043"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-0,5",
              desElement: "CSKA MOSCOW",
              idAchievement: "MDA2Ng==",
              idBet: "MDI=",
              idElement: "WDAzMzE=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16044"
            },
            {
              achievementValue: "-145",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDA2Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4ODA=",
              officialRotation: "0"
            },
            {
              achievementValue: "+115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDA2OA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4ODA=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "AC WOLFSBERGE VS CSKA MOSCOW  ",
          idEvent: "Nzc4ODA=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+225",
              conditionAchievement: null,
              desElement: "AC WOLFSBERGER",
              idAchievement: "MDA2Mg==",
              idBet: "MDE=",
              idElement: "WDE4MjU=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16043"
            },
            {
              achievementValue: "+105",
              conditionAchievement: null,
              desElement: "CSKA MOSCOW",
              idAchievement: "MDA2Mw==",
              idBet: "MDE=",
              idElement: "WDAzMzE=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16044"
            },
            {
              achievementValue: "+280",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA2NA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16045"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+0,5",
              desElement: "AC WOLFSBERGER",
              idAchievement: "MDA2NQ==",
              idBet: "MDI=",
              idElement: "WDE4MjU=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16043"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-0,5",
              desElement: "CSKA MOSCOW",
              idAchievement: "MDA2Ng==",
              idBet: "MDI=",
              idElement: "WDAzMzE=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16044"
            },
            {
              achievementValue: "-145",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDA2Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4ODA=",
              officialRotation: "0"
            },
            {
              achievementValue: "+115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDA2OA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4ODA=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "AC WOLFSBERGE VS CSKA MOSCOW  ",
          idEvent: "Nzc4ODA=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+225",
              conditionAchievement: null,
              desElement: "AC WOLFSBERGER",
              idAchievement: "MDA2Mg==",
              idBet: "MDE=",
              idElement: "WDE4MjU=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16043"
            },
            {
              achievementValue: "+105",
              conditionAchievement: null,
              desElement: "CSKA MOSCOW",
              idAchievement: "MDA2Mw==",
              idBet: "MDE=",
              idElement: "WDAzMzE=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16044"
            },
            {
              achievementValue: "+280",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA2NA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16045"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+0,5",
              desElement: "AC WOLFSBERGER",
              idAchievement: "MDA2NQ==",
              idBet: "MDI=",
              idElement: "WDE4MjU=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16043"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-0,5",
              desElement: "CSKA MOSCOW",
              idAchievement: "MDA2Ng==",
              idBet: "MDI=",
              idElement: "WDAzMzE=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16044"
            },
            {
              achievementValue: "-145",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDA2Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4ODA=",
              officialRotation: "0"
            },
            {
              achievementValue: "+115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDA2OA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4ODA=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "LIBEREC       VS KAA GENT     ",
          idEvent: "Nzc4Nzg=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+200",
              conditionAchievement: null,
              desElement: "LIBEREC",
              idAchievement: "MDA2OQ==",
              idBet: "MDE=",
              idElement: "WDA5Njk=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "16046"
            },
            {
              achievementValue: "+140",
              conditionAchievement: null,
              desElement: "KAA GENT",
              idAchievement: "MDA3MA==",
              idBet: "MDE=",
              idElement: "WDA5NjU=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "16047"
            },
            {
              achievementValue: "+225",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA3MQ==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "16048"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDA3Mg==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "0"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDA3Mw==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4Nzg=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4Nzg=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "LIBEREC       VS KAA GENT     ",
          idEvent: "Nzc4Nzg=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+200",
              conditionAchievement: null,
              desElement: "LIBEREC",
              idAchievement: "MDA2OQ==",
              idBet: "MDE=",
              idElement: "WDA5Njk=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "16046"
            },
            {
              achievementValue: "+140",
              conditionAchievement: null,
              desElement: "KAA GENT",
              idAchievement: "MDA3MA==",
              idBet: "MDE=",
              idElement: "WDA5NjU=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "16047"
            },
            {
              achievementValue: "+225",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA3MQ==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "16048"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDA3Mg==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "0"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDA3Mw==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4Nzg=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4Nzg=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "LIBEREC       VS KAA GENT     ",
          idEvent: "Nzc4Nzg=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+200",
              conditionAchievement: null,
              desElement: "LIBEREC",
              idAchievement: "MDA2OQ==",
              idBet: "MDE=",
              idElement: "WDA5Njk=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "16046"
            },
            {
              achievementValue: "+140",
              conditionAchievement: null,
              desElement: "KAA GENT",
              idAchievement: "MDA3MA==",
              idBet: "MDE=",
              idElement: "WDA5NjU=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "16047"
            },
            {
              achievementValue: "+225",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA3MQ==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "16048"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDA3Mg==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "0"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDA3Mw==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4Nzg=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4Nzg=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "LEICESTER     VS ZORYA        ",
          idEvent: "Nzc4Nzc=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "-350",
              conditionAchievement: null,
              desElement: "LEICESTER",
              idAchievement: "MDA3NA==",
              idBet: "MDE=",
              idElement: "WDAzOTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16049"
            },
            {
              achievementValue: "+1000",
              conditionAchievement: null,
              desElement: "ZORYA",
              idAchievement: "MDA3NQ==",
              idBet: "MDE=",
              idElement: "WDA2NTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16050"
            },
            {
              achievementValue: "+425",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA3Ng==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16051"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-1,5",
              desElement: "LEICESTER",
              idAchievement: "MDA3Nw==",
              idBet: "MDI=",
              idElement: "WDAzOTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16049"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+1,5",
              desElement: "ZORYA",
              idAchievement: "MDA3OA==",
              idBet: "MDI=",
              idElement: "WDA2NTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16050"
            },
            {
              achievementValue: "-145",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDA3OQ==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "0"
            },
            {
              achievementValue: "+115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDA4MA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "LEICESTER     VS ZORYA        ",
          idEvent: "Nzc4Nzc=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "-350",
              conditionAchievement: null,
              desElement: "LEICESTER",
              idAchievement: "MDA3NA==",
              idBet: "MDE=",
              idElement: "WDAzOTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16049"
            },
            {
              achievementValue: "+1000",
              conditionAchievement: null,
              desElement: "ZORYA",
              idAchievement: "MDA3NQ==",
              idBet: "MDE=",
              idElement: "WDA2NTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16050"
            },
            {
              achievementValue: "+425",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA3Ng==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16051"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-1,5",
              desElement: "LEICESTER",
              idAchievement: "MDA3Nw==",
              idBet: "MDI=",
              idElement: "WDAzOTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16049"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+1,5",
              desElement: "ZORYA",
              idAchievement: "MDA3OA==",
              idBet: "MDI=",
              idElement: "WDA2NTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16050"
            },
            {
              achievementValue: "-145",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDA3OQ==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "0"
            },
            {
              achievementValue: "+115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDA4MA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "LEICESTER     VS ZORYA        ",
          idEvent: "Nzc4Nzc=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "-350",
              conditionAchievement: null,
              desElement: "LEICESTER",
              idAchievement: "MDA3NA==",
              idBet: "MDE=",
              idElement: "WDAzOTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16049"
            },
            {
              achievementValue: "+1000",
              conditionAchievement: null,
              desElement: "ZORYA",
              idAchievement: "MDA3NQ==",
              idBet: "MDE=",
              idElement: "WDA2NTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16050"
            },
            {
              achievementValue: "+425",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA3Ng==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16051"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-1,5",
              desElement: "LEICESTER",
              idAchievement: "MDA3Nw==",
              idBet: "MDI=",
              idElement: "WDAzOTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16049"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+1,5",
              desElement: "ZORYA",
              idAchievement: "MDA3OA==",
              idBet: "MDI=",
              idElement: "WDA2NTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16050"
            },
            {
              achievementValue: "-145",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDA3OQ==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "0"
            },
            {
              achievementValue: "+115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDA4MA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "MACCABI TEL A VS QARABAG      ",
          idEvent: "Nzc4Nzk=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+110",
              conditionAchievement: null,
              desElement: "MACCABI TEL AVIV",
              idAchievement: "MDA4MQ==",
              idBet: "MDE=",
              idElement: "WDAzNTc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16052"
            },
            {
              achievementValue: "+260",
              conditionAchievement: null,
              desElement: "QARABAG",
              idAchievement: "MDA4Mg==",
              idBet: "MDE=",
              idElement: "WDA5NzQ=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16053"
            },
            {
              achievementValue: "+220",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA4Mw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16054"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-0,5",
              desElement: "MACCABI TEL AVIV",
              idAchievement: "MDA4NA==",
              idBet: "MDI=",
              idElement: "WDAzNTc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16052"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+0,5",
              desElement: "QARABAG",
              idAchievement: "MDA4NQ==",
              idBet: "MDI=",
              idElement: "WDA5NzQ=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16053"
            },
            {
              achievementValue: "-130",
              conditionAchievement: "2",
              desElement: "ALTA",
              idAchievement: "MDA4Ng==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "0"
            },
            {
              achievementValue: "+100",
              conditionAchievement: "2",
              desElement: "BAJA",
              idAchievement: "MDA4Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "MACCABI TEL A VS QARABAG      ",
          idEvent: "Nzc4Nzk=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+110",
              conditionAchievement: null,
              desElement: "MACCABI TEL AVIV",
              idAchievement: "MDA4MQ==",
              idBet: "MDE=",
              idElement: "WDAzNTc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16052"
            },
            {
              achievementValue: "+260",
              conditionAchievement: null,
              desElement: "QARABAG",
              idAchievement: "MDA4Mg==",
              idBet: "MDE=",
              idElement: "WDA5NzQ=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16053"
            },
            {
              achievementValue: "+220",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA4Mw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16054"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-0,5",
              desElement: "MACCABI TEL AVIV",
              idAchievement: "MDA4NA==",
              idBet: "MDI=",
              idElement: "WDAzNTc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16052"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+0,5",
              desElement: "QARABAG",
              idAchievement: "MDA4NQ==",
              idBet: "MDI=",
              idElement: "WDA5NzQ=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16053"
            },
            {
              achievementValue: "-130",
              conditionAchievement: "2",
              desElement: "ALTA",
              idAchievement: "MDA4Ng==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "0"
            },
            {
              achievementValue: "+100",
              conditionAchievement: "2",
              desElement: "BAJA",
              idAchievement: "MDA4Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "MACCABI TEL A VS QARABAG      ",
          idEvent: "Nzc4Nzk=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+110",
              conditionAchievement: null,
              desElement: "MACCABI TEL AVIV",
              idAchievement: "MDA4MQ==",
              idBet: "MDE=",
              idElement: "WDAzNTc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16052"
            },
            {
              achievementValue: "+260",
              conditionAchievement: null,
              desElement: "QARABAG",
              idAchievement: "MDA4Mg==",
              idBet: "MDE=",
              idElement: "WDA5NzQ=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16053"
            },
            {
              achievementValue: "+220",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA4Mw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16054"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-0,5",
              desElement: "MACCABI TEL AVIV",
              idAchievement: "MDA4NA==",
              idBet: "MDI=",
              idElement: "WDAzNTc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16052"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+0,5",
              desElement: "QARABAG",
              idAchievement: "MDA4NQ==",
              idBet: "MDI=",
              idElement: "WDA5NzQ=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16053"
            },
            {
              achievementValue: "-130",
              conditionAchievement: "2",
              desElement: "ALTA",
              idAchievement: "MDA4Ng==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "0"
            },
            {
              achievementValue: "+100",
              conditionAchievement: "2",
              desElement: "BAJA",
              idAchievement: "MDA4Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608669900",
          dateEvent: "1608670800",
          dateReprogramming: "1608674100",
          desEvent: "H BEER SHEVA  VS SLAVIA PRAGUE",
          idEvent: "Nzc4NzU=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+410",
              conditionAchievement: null,
              desElement: "H BEER SHEVA",
              idAchievement: "MDAwOA==",
              idBet: "MDE=",
              idElement: "WDExMTk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16037"
            },
            {
              achievementValue: "-160",
              conditionAchievement: null,
              desElement: "SLAVIA PRAGUE ",
              idAchievement: "MDAwOQ==",
              idBet: "MDE=",
              idElement: "WDExMjk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16038"
            },
            {
              achievementValue: "+310",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDAxMA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16039"
            },
            {
              achievementValue: "+120",
              conditionAchievement: "+0,5",
              desElement: "H BEER SHEVA",
              idAchievement: "MDAxMQ==",
              idBet: "MDI=",
              idElement: "WDExMTk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16037"
            },
            {
              achievementValue: "-160",
              conditionAchievement: "-0,5",
              desElement: "SLAVIA PRAGUE ",
              idAchievement: "MDAxMg==",
              idBet: "MDI=",
              idElement: "WDExMjk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16038"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDAxMw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4NzU=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDAxNA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4NzU=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608669900",
          dateEvent: "1608670800",
          dateReprogramming: "1608674100",
          desEvent: "H BEER SHEVA  VS SLAVIA PRAGUE",
          idEvent: "Nzc4NzU=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+410",
              conditionAchievement: null,
              desElement: "H BEER SHEVA",
              idAchievement: "MDAwOA==",
              idBet: "MDE=",
              idElement: "WDExMTk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16037"
            },
            {
              achievementValue: "-160",
              conditionAchievement: null,
              desElement: "SLAVIA PRAGUE ",
              idAchievement: "MDAwOQ==",
              idBet: "MDE=",
              idElement: "WDExMjk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16038"
            },
            {
              achievementValue: "+310",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDAxMA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16039"
            },
            {
              achievementValue: "+120",
              conditionAchievement: "+0,5",
              desElement: "H BEER SHEVA",
              idAchievement: "MDAxMQ==",
              idBet: "MDI=",
              idElement: "WDExMTk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16037"
            },
            {
              achievementValue: "-160",
              conditionAchievement: "-0,5",
              desElement: "SLAVIA PRAGUE ",
              idAchievement: "MDAxMg==",
              idBet: "MDI=",
              idElement: "WDExMjk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16038"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDAxMw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4NzU=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDAxNA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4NzU=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608669900",
          dateEvent: "1608670800",
          dateReprogramming: "1608656100",
          desEvent: "DUNDALK       VS MOLDE        ",
          idEvent: "Nzc4NzQ=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+380",
              conditionAchievement: null,
              desElement: "DUNDALK",
              idAchievement: "MDAxNQ==",
              idBet: "MDE=",
              idElement: "WDExMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16040"
            },
            {
              achievementValue: "-180",
              conditionAchievement: null,
              desElement: "MOLDE",
              idAchievement: "MDAxNg==",
              idBet: "MDE=",
              idElement: "WDA5Njc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16041"
            },
            {
              achievementValue: "+335",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDAxNw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16042"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+1",
              desElement: "DUNDALK",
              idAchievement: "MDAxOA==",
              idBet: "MDI=",
              idElement: "WDExMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16040"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-1",
              desElement: "MOLDE",
              idAchievement: "MDAxOQ==",
              idBet: "MDI=",
              idElement: "WDA5Njc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16041"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "ALTA",
              idAchievement: "MDAyMA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "BAJA",
              idAchievement: "MDAyMQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608669900",
          dateEvent: "1608670800",
          dateReprogramming: "1608656100",
          desEvent: "DUNDALK       VS MOLDE        ",
          idEvent: "Nzc4NzQ=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+380",
              conditionAchievement: null,
              desElement: "DUNDALK",
              idAchievement: "MDAxNQ==",
              idBet: "MDE=",
              idElement: "WDExMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16040"
            },
            {
              achievementValue: "-180",
              conditionAchievement: null,
              desElement: "MOLDE",
              idAchievement: "MDAxNg==",
              idBet: "MDE=",
              idElement: "WDA5Njc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16041"
            },
            {
              achievementValue: "+335",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDAxNw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16042"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+1",
              desElement: "DUNDALK",
              idAchievement: "MDAxOA==",
              idBet: "MDI=",
              idElement: "WDExMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16040"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-1",
              desElement: "MOLDE",
              idAchievement: "MDAxOQ==",
              idBet: "MDI=",
              idElement: "WDA5Njc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16041"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "ALTA",
              idAchievement: "MDAyMA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "BAJA",
              idAchievement: "MDAyMQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "AC WOLFSBERGE VS CSKA MOSCOW  ",
          idEvent: "Nzc4ODA=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+225",
              conditionAchievement: null,
              desElement: "AC WOLFSBERGER",
              idAchievement: "MDA2Mg==",
              idBet: "MDE=",
              idElement: "WDE4MjU=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16043"
            },
            {
              achievementValue: "+105",
              conditionAchievement: null,
              desElement: "CSKA MOSCOW",
              idAchievement: "MDA2Mw==",
              idBet: "MDE=",
              idElement: "WDAzMzE=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16044"
            },
            {
              achievementValue: "+280",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA2NA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16045"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+0,5",
              desElement: "AC WOLFSBERGER",
              idAchievement: "MDA2NQ==",
              idBet: "MDI=",
              idElement: "WDE4MjU=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16043"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-0,5",
              desElement: "CSKA MOSCOW",
              idAchievement: "MDA2Ng==",
              idBet: "MDI=",
              idElement: "WDAzMzE=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16044"
            },
            {
              achievementValue: "-145",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDA2Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4ODA=",
              officialRotation: "0"
            },
            {
              achievementValue: "+115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDA2OA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4ODA=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "AC WOLFSBERGE VS CSKA MOSCOW  ",
          idEvent: "Nzc4ODA=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+225",
              conditionAchievement: null,
              desElement: "AC WOLFSBERGER",
              idAchievement: "MDA2Mg==",
              idBet: "MDE=",
              idElement: "WDE4MjU=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16043"
            },
            {
              achievementValue: "+105",
              conditionAchievement: null,
              desElement: "CSKA MOSCOW",
              idAchievement: "MDA2Mw==",
              idBet: "MDE=",
              idElement: "WDAzMzE=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16044"
            },
            {
              achievementValue: "+280",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA2NA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16045"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+0,5",
              desElement: "AC WOLFSBERGER",
              idAchievement: "MDA2NQ==",
              idBet: "MDI=",
              idElement: "WDE4MjU=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16043"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-0,5",
              desElement: "CSKA MOSCOW",
              idAchievement: "MDA2Ng==",
              idBet: "MDI=",
              idElement: "WDAzMzE=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16044"
            },
            {
              achievementValue: "-145",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDA2Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4ODA=",
              officialRotation: "0"
            },
            {
              achievementValue: "+115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDA2OA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4ODA=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "LEICESTER     VS ZORYA        ",
          idEvent: "Nzc4Nzc=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "-350",
              conditionAchievement: null,
              desElement: "LEICESTER",
              idAchievement: "MDA3NA==",
              idBet: "MDE=",
              idElement: "WDAzOTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16049"
            },
            {
              achievementValue: "+1000",
              conditionAchievement: null,
              desElement: "ZORYA",
              idAchievement: "MDA3NQ==",
              idBet: "MDE=",
              idElement: "WDA2NTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16050"
            },
            {
              achievementValue: "+425",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA3Ng==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16051"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-1,5",
              desElement: "LEICESTER",
              idAchievement: "MDA3Nw==",
              idBet: "MDI=",
              idElement: "WDAzOTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16049"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+1,5",
              desElement: "ZORYA",
              idAchievement: "MDA3OA==",
              idBet: "MDI=",
              idElement: "WDA2NTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16050"
            },
            {
              achievementValue: "-145",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDA3OQ==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "0"
            },
            {
              achievementValue: "+115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDA4MA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "LEICESTER     VS ZORYA        ",
          idEvent: "Nzc4Nzc=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "-350",
              conditionAchievement: null,
              desElement: "LEICESTER",
              idAchievement: "MDA3NA==",
              idBet: "MDE=",
              idElement: "WDAzOTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16049"
            },
            {
              achievementValue: "+1000",
              conditionAchievement: null,
              desElement: "ZORYA",
              idAchievement: "MDA3NQ==",
              idBet: "MDE=",
              idElement: "WDA2NTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16050"
            },
            {
              achievementValue: "+425",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA3Ng==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16051"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-1,5",
              desElement: "LEICESTER",
              idAchievement: "MDA3Nw==",
              idBet: "MDI=",
              idElement: "WDAzOTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16049"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+1,5",
              desElement: "ZORYA",
              idAchievement: "MDA3OA==",
              idBet: "MDI=",
              idElement: "WDA2NTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16050"
            },
            {
              achievementValue: "-145",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDA3OQ==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "0"
            },
            {
              achievementValue: "+115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDA4MA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "MACCABI TEL A VS QARABAG      ",
          idEvent: "Nzc4Nzk=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+110",
              conditionAchievement: null,
              desElement: "MACCABI TEL AVIV",
              idAchievement: "MDA4MQ==",
              idBet: "MDE=",
              idElement: "WDAzNTc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16052"
            },
            {
              achievementValue: "+260",
              conditionAchievement: null,
              desElement: "QARABAG",
              idAchievement: "MDA4Mg==",
              idBet: "MDE=",
              idElement: "WDA5NzQ=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16053"
            },
            {
              achievementValue: "+220",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA4Mw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16054"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-0,5",
              desElement: "MACCABI TEL AVIV",
              idAchievement: "MDA4NA==",
              idBet: "MDI=",
              idElement: "WDAzNTc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16052"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+0,5",
              desElement: "QARABAG",
              idAchievement: "MDA4NQ==",
              idBet: "MDI=",
              idElement: "WDA5NzQ=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16053"
            },
            {
              achievementValue: "-130",
              conditionAchievement: "2",
              desElement: "ALTA",
              idAchievement: "MDA4Ng==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "0"
            },
            {
              achievementValue: "+100",
              conditionAchievement: "2",
              desElement: "BAJA",
              idAchievement: "MDA4Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "MACCABI TEL A VS QARABAG      ",
          idEvent: "Nzc4Nzk=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+110",
              conditionAchievement: null,
              desElement: "MACCABI TEL AVIV",
              idAchievement: "MDA4MQ==",
              idBet: "MDE=",
              idElement: "WDAzNTc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16052"
            },
            {
              achievementValue: "+260",
              conditionAchievement: null,
              desElement: "QARABAG",
              idAchievement: "MDA4Mg==",
              idBet: "MDE=",
              idElement: "WDA5NzQ=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16053"
            },
            {
              achievementValue: "+220",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA4Mw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16054"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-0,5",
              desElement: "MACCABI TEL AVIV",
              idAchievement: "MDA4NA==",
              idBet: "MDI=",
              idElement: "WDAzNTc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16052"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+0,5",
              desElement: "QARABAG",
              idAchievement: "MDA4NQ==",
              idBet: "MDI=",
              idElement: "WDA5NzQ=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16053"
            },
            {
              achievementValue: "-130",
              conditionAchievement: "2",
              desElement: "ALTA",
              idAchievement: "MDA4Ng==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "0"
            },
            {
              achievementValue: "+100",
              conditionAchievement: "2",
              desElement: "BAJA",
              idAchievement: "MDA4Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "MACCABI TEL A VS QARABAG      ",
          idEvent: "Nzc4Nzk=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+110",
              conditionAchievement: null,
              desElement: "MACCABI TEL AVIV",
              idAchievement: "MDA4MQ==",
              idBet: "MDE=",
              idElement: "WDAzNTc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16052"
            },
            {
              achievementValue: "+260",
              conditionAchievement: null,
              desElement: "QARABAG",
              idAchievement: "MDA4Mg==",
              idBet: "MDE=",
              idElement: "WDA5NzQ=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16053"
            },
            {
              achievementValue: "+220",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA4Mw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16054"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-0,5",
              desElement: "MACCABI TEL AVIV",
              idAchievement: "MDA4NA==",
              idBet: "MDI=",
              idElement: "WDAzNTc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16052"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+0,5",
              desElement: "QARABAG",
              idAchievement: "MDA4NQ==",
              idBet: "MDI=",
              idElement: "WDA5NzQ=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16053"
            },
            {
              achievementValue: "-130",
              conditionAchievement: "2",
              desElement: "ALTA",
              idAchievement: "MDA4Ng==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "0"
            },
            {
              achievementValue: "+100",
              conditionAchievement: "2",
              desElement: "BAJA",
              idAchievement: "MDA4Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "LEICESTER     VS ZORYA        ",
          idEvent: "Nzc4Nzc=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "-350",
              conditionAchievement: null,
              desElement: "LEICESTER",
              idAchievement: "MDA3NA==",
              idBet: "MDE=",
              idElement: "WDAzOTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16049"
            },
            {
              achievementValue: "+1000",
              conditionAchievement: null,
              desElement: "ZORYA",
              idAchievement: "MDA3NQ==",
              idBet: "MDE=",
              idElement: "WDA2NTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16050"
            },
            {
              achievementValue: "+425",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA3Ng==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16051"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-1,5",
              desElement: "LEICESTER",
              idAchievement: "MDA3Nw==",
              idBet: "MDI=",
              idElement: "WDAzOTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16049"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+1,5",
              desElement: "ZORYA",
              idAchievement: "MDA3OA==",
              idBet: "MDI=",
              idElement: "WDA2NTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16050"
            },
            {
              achievementValue: "-145",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDA3OQ==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "0"
            },
            {
              achievementValue: "+115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDA4MA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "LIBEREC       VS KAA GENT     ",
          idEvent: "Nzc4Nzg=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+200",
              conditionAchievement: null,
              desElement: "LIBEREC",
              idAchievement: "MDA2OQ==",
              idBet: "MDE=",
              idElement: "WDA5Njk=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "16046"
            },
            {
              achievementValue: "+140",
              conditionAchievement: null,
              desElement: "KAA GENT",
              idAchievement: "MDA3MA==",
              idBet: "MDE=",
              idElement: "WDA5NjU=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "16047"
            },
            {
              achievementValue: "+225",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA3MQ==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "16048"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDA3Mg==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "0"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDA3Mw==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4Nzg=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4Nzg=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "AC WOLFSBERGE VS CSKA MOSCOW  ",
          idEvent: "Nzc4ODA=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+225",
              conditionAchievement: null,
              desElement: "AC WOLFSBERGER",
              idAchievement: "MDA2Mg==",
              idBet: "MDE=",
              idElement: "WDE4MjU=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16043"
            },
            {
              achievementValue: "+105",
              conditionAchievement: null,
              desElement: "CSKA MOSCOW",
              idAchievement: "MDA2Mw==",
              idBet: "MDE=",
              idElement: "WDAzMzE=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16044"
            },
            {
              achievementValue: "+280",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA2NA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16045"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+0,5",
              desElement: "AC WOLFSBERGER",
              idAchievement: "MDA2NQ==",
              idBet: "MDI=",
              idElement: "WDE4MjU=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16043"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-0,5",
              desElement: "CSKA MOSCOW",
              idAchievement: "MDA2Ng==",
              idBet: "MDI=",
              idElement: "WDAzMzE=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16044"
            },
            {
              achievementValue: "-145",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDA2Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4ODA=",
              officialRotation: "0"
            },
            {
              achievementValue: "+115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDA2OA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4ODA=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608669900",
          dateEvent: "1608670800",
          dateReprogramming: "1608656100",
          desEvent: "PAOK SALONIKA VS OMONIA NICOSI",
          idEvent: "Nzc4NzY=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "-115",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDAwNg==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4NzY=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDAwNw==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4NzY=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDM=",
              idEvent: "Nzc4NzY=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608669900",
          dateEvent: "1608670800",
          dateReprogramming: "1608656100",
          desEvent: "DUNDALK       VS MOLDE        ",
          idEvent: "Nzc4NzQ=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+380",
              conditionAchievement: null,
              desElement: "DUNDALK",
              idAchievement: "MDAxNQ==",
              idBet: "MDE=",
              idElement: "WDExMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16040"
            },
            {
              achievementValue: "-180",
              conditionAchievement: null,
              desElement: "MOLDE",
              idAchievement: "MDAxNg==",
              idBet: "MDE=",
              idElement: "WDA5Njc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16041"
            },
            {
              achievementValue: "+335",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDAxNw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16042"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+1",
              desElement: "DUNDALK",
              idAchievement: "MDAxOA==",
              idBet: "MDI=",
              idElement: "WDExMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16040"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-1",
              desElement: "MOLDE",
              idAchievement: "MDAxOQ==",
              idBet: "MDI=",
              idElement: "WDA5Njc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16041"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "ALTA",
              idAchievement: "MDAyMA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "BAJA",
              idAchievement: "MDAyMQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608669900",
          dateEvent: "1608670800",
          dateReprogramming: "1608674100",
          desEvent: "H BEER SHEVA  VS SLAVIA PRAGUE",
          idEvent: "Nzc4NzU=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+410",
              conditionAchievement: null,
              desElement: "H BEER SHEVA",
              idAchievement: "MDAwOA==",
              idBet: "MDE=",
              idElement: "WDExMTk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16037"
            },
            {
              achievementValue: "-160",
              conditionAchievement: null,
              desElement: "SLAVIA PRAGUE ",
              idAchievement: "MDAwOQ==",
              idBet: "MDE=",
              idElement: "WDExMjk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16038"
            },
            {
              achievementValue: "+310",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDAxMA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16039"
            },
            {
              achievementValue: "+120",
              conditionAchievement: "+0,5",
              desElement: "H BEER SHEVA",
              idAchievement: "MDAxMQ==",
              idBet: "MDI=",
              idElement: "WDExMTk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16037"
            },
            {
              achievementValue: "-160",
              conditionAchievement: "-0,5",
              desElement: "SLAVIA PRAGUE ",
              idAchievement: "MDAxMg==",
              idBet: "MDI=",
              idElement: "WDExMjk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16038"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDAxMw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4NzU=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDAxNA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4NzU=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "AC WOLFSBERGE VS CSKA MOSCOW  ",
          idEvent: "Nzc4ODA=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+225",
              conditionAchievement: null,
              desElement: "AC WOLFSBERGER",
              idAchievement: "MDA2Mg==",
              idBet: "MDE=",
              idElement: "WDE4MjU=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16043"
            },
            {
              achievementValue: "+105",
              conditionAchievement: null,
              desElement: "CSKA MOSCOW",
              idAchievement: "MDA2Mw==",
              idBet: "MDE=",
              idElement: "WDAzMzE=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16044"
            },
            {
              achievementValue: "+280",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA2NA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16045"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+0,5",
              desElement: "AC WOLFSBERGER",
              idAchievement: "MDA2NQ==",
              idBet: "MDI=",
              idElement: "WDE4MjU=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16043"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-0,5",
              desElement: "CSKA MOSCOW",
              idAchievement: "MDA2Ng==",
              idBet: "MDI=",
              idElement: "WDAzMzE=",
              idEvent: "Nzc4ODA=",
              officialRotation: "16044"
            },
            {
              achievementValue: "-145",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDA2Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4ODA=",
              officialRotation: "0"
            },
            {
              achievementValue: "+115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDA2OA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4ODA=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4ODA=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608669900",
          dateEvent: "1608670800",
          dateReprogramming: "1608674100",
          desEvent: "H BEER SHEVA  VS SLAVIA PRAGUE",
          idEvent: "Nzc4NzU=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+410",
              conditionAchievement: null,
              desElement: "H BEER SHEVA",
              idAchievement: "MDAwOA==",
              idBet: "MDE=",
              idElement: "WDExMTk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16037"
            },
            {
              achievementValue: "-160",
              conditionAchievement: null,
              desElement: "SLAVIA PRAGUE ",
              idAchievement: "MDAwOQ==",
              idBet: "MDE=",
              idElement: "WDExMjk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16038"
            },
            {
              achievementValue: "+310",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDAxMA==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16039"
            },
            {
              achievementValue: "+120",
              conditionAchievement: "+0,5",
              desElement: "H BEER SHEVA",
              idAchievement: "MDAxMQ==",
              idBet: "MDI=",
              idElement: "WDExMTk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16037"
            },
            {
              achievementValue: "-160",
              conditionAchievement: "-0,5",
              desElement: "SLAVIA PRAGUE ",
              idAchievement: "MDAxMg==",
              idBet: "MDI=",
              idElement: "WDExMjk=",
              idEvent: "Nzc4NzU=",
              officialRotation: "16038"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDAxMw==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4NzU=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDAxNA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4NzU=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4NzU=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "LEICESTER     VS ZORYA        ",
          idEvent: "Nzc4Nzc=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "-350",
              conditionAchievement: null,
              desElement: "LEICESTER",
              idAchievement: "MDA3NA==",
              idBet: "MDE=",
              idElement: "WDAzOTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16049"
            },
            {
              achievementValue: "+1000",
              conditionAchievement: null,
              desElement: "ZORYA",
              idAchievement: "MDA3NQ==",
              idBet: "MDE=",
              idElement: "WDA2NTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16050"
            },
            {
              achievementValue: "+425",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA3Ng==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16051"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-1,5",
              desElement: "LEICESTER",
              idAchievement: "MDA3Nw==",
              idBet: "MDI=",
              idElement: "WDAzOTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16049"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+1,5",
              desElement: "ZORYA",
              idAchievement: "MDA3OA==",
              idBet: "MDI=",
              idElement: "WDA2NTY=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "16050"
            },
            {
              achievementValue: "-145",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDA3OQ==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "0"
            },
            {
              achievementValue: "+115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDA4MA==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4Nzc=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4Nzc=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608669900",
          dateEvent: "1608670800",
          dateReprogramming: "1608656100",
          desEvent: "DUNDALK       VS MOLDE        ",
          idEvent: "Nzc4NzQ=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+380",
              conditionAchievement: null,
              desElement: "DUNDALK",
              idAchievement: "MDAxNQ==",
              idBet: "MDE=",
              idElement: "WDExMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16040"
            },
            {
              achievementValue: "-180",
              conditionAchievement: null,
              desElement: "MOLDE",
              idAchievement: "MDAxNg==",
              idBet: "MDE=",
              idElement: "WDA5Njc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16041"
            },
            {
              achievementValue: "+335",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDAxNw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16042"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+1",
              desElement: "DUNDALK",
              idAchievement: "MDAxOA==",
              idBet: "MDI=",
              idElement: "WDExMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16040"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-1",
              desElement: "MOLDE",
              idAchievement: "MDAxOQ==",
              idBet: "MDI=",
              idElement: "WDA5Njc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "16041"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "ALTA",
              idAchievement: "MDAyMA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "3",
              desElement: "BAJA",
              idAchievement: "MDAyMQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4NzQ=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4NzQ=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "LIBEREC       VS KAA GENT     ",
          idEvent: "Nzc4Nzg=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+200",
              conditionAchievement: null,
              desElement: "LIBEREC",
              idAchievement: "MDA2OQ==",
              idBet: "MDE=",
              idElement: "WDA5Njk=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "16046"
            },
            {
              achievementValue: "+140",
              conditionAchievement: null,
              desElement: "KAA GENT",
              idAchievement: "MDA3MA==",
              idBet: "MDE=",
              idElement: "WDA5NjU=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "16047"
            },
            {
              achievementValue: "+225",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA3MQ==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "16048"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDA3Mg==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "0"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDA3Mw==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4Nzg=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4Nzg=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4Nzg=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608670200",
          dateEvent: "1608670800",
          dateReprogramming: "1608670800",
          desEvent: "MACCABI TEL A VS QARABAG      ",
          idEvent: "Nzc4Nzk=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+110",
              conditionAchievement: null,
              desElement: "MACCABI TEL AVIV",
              idAchievement: "MDA4MQ==",
              idBet: "MDE=",
              idElement: "WDAzNTc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16052"
            },
            {
              achievementValue: "+260",
              conditionAchievement: null,
              desElement: "QARABAG",
              idAchievement: "MDA4Mg==",
              idBet: "MDE=",
              idElement: "WDA5NzQ=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16053"
            },
            {
              achievementValue: "+220",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDA4Mw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16054"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "-0,5",
              desElement: "MACCABI TEL AVIV",
              idAchievement: "MDA4NA==",
              idBet: "MDI=",
              idElement: "WDAzNTc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16052"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "+0,5",
              desElement: "QARABAG",
              idAchievement: "MDA4NQ==",
              idBet: "MDI=",
              idElement: "WDA5NzQ=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "16053"
            },
            {
              achievementValue: "-130",
              conditionAchievement: "2",
              desElement: "ALTA",
              idAchievement: "MDA4Ng==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "0"
            },
            {
              achievementValue: "+100",
              conditionAchievement: "2",
              desElement: "BAJA",
              idAchievement: "MDA4Nw==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4Nzk=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc4Nzk=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608669900",
          dateEvent: "1608670800",
          dateReprogramming: "1608656100",
          desEvent: "PAOK SALONIKA VS OMONIA NICOSI",
          idEvent: "Nzc4NzY=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "-115",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDAwNg==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc4NzY=",
              officialRotation: "0"
            },
            {
              achievementValue: "-115",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDAwNw==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc4NzY=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDM=",
              idEvent: "Nzc4NzY=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608673500",
          dateEvent: "1608674100",
          dateReprogramming: "1608674100",
          desEvent: "LECH POZNAN   VS BENFICA      ",
          idEvent: "Nzc2ODQ=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+600",
              conditionAchievement: null,
              desElement: "LECH POZNAN",
              idAchievement: "MDEzNQ==",
              idBet: "MDE=",
              idElement: "WDA5NzI=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15961"
            },
            {
              achievementValue: "-235",
              conditionAchievement: null,
              desElement: "BENFICA",
              idAchievement: "MDEzNg==",
              idBet: "MDE=",
              idElement: "WDAyNTE=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15962"
            },
            {
              achievementValue: "+365",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDEzNw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15963"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "+1",
              desElement: "LECH POZNAN",
              idAchievement: "MDEzOA==",
              idBet: "MDI=",
              idElement: "WDA5NzI=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15961"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "-1",
              desElement: "BENFICA",
              idAchievement: "MDEzOQ==",
              idBet: "MDI=",
              idElement: "WDAyNTE=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15962"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDE0MA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "0"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDE0MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608673500",
          dateEvent: "1608674100",
          dateReprogramming: "1608674100",
          desEvent: "LECH POZNAN   VS BENFICA      ",
          idEvent: "Nzc2ODQ=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+600",
              conditionAchievement: null,
              desElement: "LECH POZNAN",
              idAchievement: "MDEzNQ==",
              idBet: "MDE=",
              idElement: "WDA5NzI=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15961"
            },
            {
              achievementValue: "-235",
              conditionAchievement: null,
              desElement: "BENFICA",
              idAchievement: "MDEzNg==",
              idBet: "MDE=",
              idElement: "WDAyNTE=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15962"
            },
            {
              achievementValue: "+365",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDEzNw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15963"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "+1",
              desElement: "LECH POZNAN",
              idAchievement: "MDEzOA==",
              idBet: "MDI=",
              idElement: "WDA5NzI=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15961"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "-1",
              desElement: "BENFICA",
              idAchievement: "MDEzOQ==",
              idBet: "MDI=",
              idElement: "WDAyNTE=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15962"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDE0MA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "0"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDE0MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608673500",
          dateEvent: "1608674100",
          dateReprogramming: "1608674100",
          desEvent: "LECH POZNAN   VS BENFICA      ",
          idEvent: "Nzc2ODQ=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+600",
              conditionAchievement: null,
              desElement: "LECH POZNAN",
              idAchievement: "MDEzNQ==",
              idBet: "MDE=",
              idElement: "WDA5NzI=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15961"
            },
            {
              achievementValue: "-235",
              conditionAchievement: null,
              desElement: "BENFICA",
              idAchievement: "MDEzNg==",
              idBet: "MDE=",
              idElement: "WDAyNTE=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15962"
            },
            {
              achievementValue: "+365",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDEzNw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15963"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "+1",
              desElement: "LECH POZNAN",
              idAchievement: "MDEzOA==",
              idBet: "MDI=",
              idElement: "WDA5NzI=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15961"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "-1",
              desElement: "BENFICA",
              idAchievement: "MDEzOQ==",
              idBet: "MDI=",
              idElement: "WDAyNTE=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15962"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDE0MA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "0"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDE0MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608673500",
          dateEvent: "1608674100",
          dateReprogramming: "1608674100",
          desEvent: "YOUNG BOYS    VS AS ROMA      ",
          idEvent: "Nzc2OTA=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+280",
              conditionAchievement: null,
              desElement: "YOUNG BOYS",
              idAchievement: "MDE3NQ==",
              idBet: "MDE=",
              idElement: "WDA2NTQ=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15979"
            },
            {
              achievementValue: "-105",
              conditionAchievement: null,
              desElement: "AS ROMA",
              idAchievement: "MDE3Ng==",
              idBet: "MDE=",
              idElement: "WDAxNDM=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15980"
            },
            {
              achievementValue: "+250",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE3Nw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15981"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+0,5",
              desElement: "YOUNG BOYS",
              idAchievement: "MDE3OA==",
              idBet: "MDI=",
              idElement: "WDA2NTQ=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15979"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-0,5",
              desElement: "AS ROMA",
              idAchievement: "MDE3OQ==",
              idBet: "MDI=",
              idElement: "WDAxNDM=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15980"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDE4MA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2OTA=",
              officialRotation: "0"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDE4MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2OTA=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608673500",
          dateEvent: "1608674100",
          dateReprogramming: "1608674100",
          desEvent: "YOUNG BOYS    VS AS ROMA      ",
          idEvent: "Nzc2OTA=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+280",
              conditionAchievement: null,
              desElement: "YOUNG BOYS",
              idAchievement: "MDE3NQ==",
              idBet: "MDE=",
              idElement: "WDA2NTQ=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15979"
            },
            {
              achievementValue: "-105",
              conditionAchievement: null,
              desElement: "AS ROMA",
              idAchievement: "MDE3Ng==",
              idBet: "MDE=",
              idElement: "WDAxNDM=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15980"
            },
            {
              achievementValue: "+250",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE3Nw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15981"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+0,5",
              desElement: "YOUNG BOYS",
              idAchievement: "MDE3OA==",
              idBet: "MDI=",
              idElement: "WDA2NTQ=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15979"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-0,5",
              desElement: "AS ROMA",
              idAchievement: "MDE3OQ==",
              idBet: "MDI=",
              idElement: "WDAxNDM=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15980"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDE4MA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2OTA=",
              officialRotation: "0"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDE4MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2OTA=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608673500",
          dateEvent: "1608674100",
          dateReprogramming: "1608674100",
          desEvent: "YOUNG BOYS    VS AS ROMA      ",
          idEvent: "Nzc2OTA=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+280",
              conditionAchievement: null,
              desElement: "YOUNG BOYS",
              idAchievement: "MDE3NQ==",
              idBet: "MDE=",
              idElement: "WDA2NTQ=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15979"
            },
            {
              achievementValue: "-105",
              conditionAchievement: null,
              desElement: "AS ROMA",
              idAchievement: "MDE3Ng==",
              idBet: "MDE=",
              idElement: "WDAxNDM=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15980"
            },
            {
              achievementValue: "+250",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE3Nw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15981"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+0,5",
              desElement: "YOUNG BOYS",
              idAchievement: "MDE3OA==",
              idBet: "MDI=",
              idElement: "WDA2NTQ=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15979"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-0,5",
              desElement: "AS ROMA",
              idAchievement: "MDE3OQ==",
              idBet: "MDI=",
              idElement: "WDAxNDM=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15980"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDE4MA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2OTA=",
              officialRotation: "0"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDE4MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2OTA=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608673500",
          dateEvent: "1608674100",
          dateReprogramming: "1608674100",
          desEvent: "LECH POZNAN   VS BENFICA      ",
          idEvent: "Nzc2ODQ=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+600",
              conditionAchievement: null,
              desElement: "LECH POZNAN",
              idAchievement: "MDEzNQ==",
              idBet: "MDE=",
              idElement: "WDA5NzI=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15961"
            },
            {
              achievementValue: "-235",
              conditionAchievement: null,
              desElement: "BENFICA",
              idAchievement: "MDEzNg==",
              idBet: "MDE=",
              idElement: "WDAyNTE=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15962"
            },
            {
              achievementValue: "+365",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDEzNw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15963"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "+1",
              desElement: "LECH POZNAN",
              idAchievement: "MDEzOA==",
              idBet: "MDI=",
              idElement: "WDA5NzI=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15961"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "-1",
              desElement: "BENFICA",
              idAchievement: "MDEzOQ==",
              idBet: "MDI=",
              idElement: "WDAyNTE=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15962"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDE0MA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "0"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDE0MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608673500",
          dateEvent: "1608674100",
          dateReprogramming: "1608674100",
          desEvent: "LECH POZNAN   VS BENFICA      ",
          idEvent: "Nzc2ODQ=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+600",
              conditionAchievement: null,
              desElement: "LECH POZNAN",
              idAchievement: "MDEzNQ==",
              idBet: "MDE=",
              idElement: "WDA5NzI=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15961"
            },
            {
              achievementValue: "-235",
              conditionAchievement: null,
              desElement: "BENFICA",
              idAchievement: "MDEzNg==",
              idBet: "MDE=",
              idElement: "WDAyNTE=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15962"
            },
            {
              achievementValue: "+365",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDEzNw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15963"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "+1",
              desElement: "LECH POZNAN",
              idAchievement: "MDEzOA==",
              idBet: "MDI=",
              idElement: "WDA5NzI=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15961"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "-1",
              desElement: "BENFICA",
              idAchievement: "MDEzOQ==",
              idBet: "MDI=",
              idElement: "WDAyNTE=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15962"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDE0MA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "0"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDE0MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608673500",
          dateEvent: "1608674100",
          dateReprogramming: "1608674100",
          desEvent: "YOUNG BOYS    VS AS ROMA      ",
          idEvent: "Nzc2OTA=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+280",
              conditionAchievement: null,
              desElement: "YOUNG BOYS",
              idAchievement: "MDE3NQ==",
              idBet: "MDE=",
              idElement: "WDA2NTQ=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15979"
            },
            {
              achievementValue: "-105",
              conditionAchievement: null,
              desElement: "AS ROMA",
              idAchievement: "MDE3Ng==",
              idBet: "MDE=",
              idElement: "WDAxNDM=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15980"
            },
            {
              achievementValue: "+250",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE3Nw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15981"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+0,5",
              desElement: "YOUNG BOYS",
              idAchievement: "MDE3OA==",
              idBet: "MDI=",
              idElement: "WDA2NTQ=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15979"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-0,5",
              desElement: "AS ROMA",
              idAchievement: "MDE3OQ==",
              idBet: "MDI=",
              idElement: "WDAxNDM=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15980"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDE4MA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2OTA=",
              officialRotation: "0"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDE4MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2OTA=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608673500",
          dateEvent: "1608674100",
          dateReprogramming: "1608674100",
          desEvent: "YOUNG BOYS    VS AS ROMA      ",
          idEvent: "Nzc2OTA=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+280",
              conditionAchievement: null,
              desElement: "YOUNG BOYS",
              idAchievement: "MDE3NQ==",
              idBet: "MDE=",
              idElement: "WDA2NTQ=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15979"
            },
            {
              achievementValue: "-105",
              conditionAchievement: null,
              desElement: "AS ROMA",
              idAchievement: "MDE3Ng==",
              idBet: "MDE=",
              idElement: "WDAxNDM=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15980"
            },
            {
              achievementValue: "+250",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE3Nw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15981"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+0,5",
              desElement: "YOUNG BOYS",
              idAchievement: "MDE3OA==",
              idBet: "MDI=",
              idElement: "WDA2NTQ=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15979"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-0,5",
              desElement: "AS ROMA",
              idAchievement: "MDE3OQ==",
              idBet: "MDI=",
              idElement: "WDAxNDM=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15980"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDE4MA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2OTA=",
              officialRotation: "0"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDE4MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2OTA=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608673500",
          dateEvent: "1608674100",
          dateReprogramming: "1608674100",
          desEvent: "LECH POZNAN   VS BENFICA      ",
          idEvent: "Nzc2ODQ=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+600",
              conditionAchievement: null,
              desElement: "LECH POZNAN",
              idAchievement: "MDEzNQ==",
              idBet: "MDE=",
              idElement: "WDA5NzI=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15961"
            },
            {
              achievementValue: "-235",
              conditionAchievement: null,
              desElement: "BENFICA",
              idAchievement: "MDEzNg==",
              idBet: "MDE=",
              idElement: "WDAyNTE=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15962"
            },
            {
              achievementValue: "+365",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDEzNw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15963"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "+1",
              desElement: "LECH POZNAN",
              idAchievement: "MDEzOA==",
              idBet: "MDI=",
              idElement: "WDA5NzI=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15961"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "-1",
              desElement: "BENFICA",
              idAchievement: "MDEzOQ==",
              idBet: "MDI=",
              idElement: "WDAyNTE=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15962"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDE0MA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "0"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDE0MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608673500",
          dateEvent: "1608674100",
          dateReprogramming: "1608674100",
          desEvent: "YOUNG BOYS    VS AS ROMA      ",
          idEvent: "Nzc2OTA=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+280",
              conditionAchievement: null,
              desElement: "YOUNG BOYS",
              idAchievement: "MDE3NQ==",
              idBet: "MDE=",
              idElement: "WDA2NTQ=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15979"
            },
            {
              achievementValue: "-105",
              conditionAchievement: null,
              desElement: "AS ROMA",
              idAchievement: "MDE3Ng==",
              idBet: "MDE=",
              idElement: "WDAxNDM=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15980"
            },
            {
              achievementValue: "+250",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE3Nw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15981"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+0,5",
              desElement: "YOUNG BOYS",
              idAchievement: "MDE3OA==",
              idBet: "MDI=",
              idElement: "WDA2NTQ=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15979"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-0,5",
              desElement: "AS ROMA",
              idAchievement: "MDE3OQ==",
              idBet: "MDI=",
              idElement: "WDAxNDM=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15980"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDE4MA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2OTA=",
              officialRotation: "0"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDE4MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2OTA=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608673500",
          dateEvent: "1608674100",
          dateReprogramming: "1608674100",
          desEvent: "LECH POZNAN   VS BENFICA      ",
          idEvent: "Nzc2ODQ=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+600",
              conditionAchievement: null,
              desElement: "LECH POZNAN",
              idAchievement: "MDEzNQ==",
              idBet: "MDE=",
              idElement: "WDA5NzI=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15961"
            },
            {
              achievementValue: "-235",
              conditionAchievement: null,
              desElement: "BENFICA",
              idAchievement: "MDEzNg==",
              idBet: "MDE=",
              idElement: "WDAyNTE=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15962"
            },
            {
              achievementValue: "+365",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDEzNw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15963"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "+1",
              desElement: "LECH POZNAN",
              idAchievement: "MDEzOA==",
              idBet: "MDI=",
              idElement: "WDA5NzI=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15961"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "-1",
              desElement: "BENFICA",
              idAchievement: "MDEzOQ==",
              idBet: "MDI=",
              idElement: "WDAyNTE=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "15962"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDE0MA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "0"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDE0MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2ODQ=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2ODQ=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        },
        {
          dateClosing: "1608673500",
          dateEvent: "1608674100",
          dateReprogramming: "1608674100",
          desEvent: "YOUNG BOYS    VS AS ROMA      ",
          idEvent: "Nzc2OTA=",
          idLeague: "MjE5",
          idSport: "MDI=",
          lstAchievement: [
            {
              achievementValue: "+280",
              conditionAchievement: null,
              desElement: "YOUNG BOYS",
              idAchievement: "MDE3NQ==",
              idBet: "MDE=",
              idElement: "WDA2NTQ=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15979"
            },
            {
              achievementValue: "-105",
              conditionAchievement: null,
              desElement: "AS ROMA",
              idAchievement: "MDE3Ng==",
              idBet: "MDE=",
              idElement: "WDAxNDM=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15980"
            },
            {
              achievementValue: "+250",
              conditionAchievement: null,
              desElement: "EMPATE",
              idAchievement: "MDE3Nw==",
              idBet: "MDE=",
              idElement: "MDAwMDk=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15981"
            },
            {
              achievementValue: "-110",
              conditionAchievement: "+0,5",
              desElement: "YOUNG BOYS",
              idAchievement: "MDE3OA==",
              idBet: "MDI=",
              idElement: "WDA2NTQ=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15979"
            },
            {
              achievementValue: "-120",
              conditionAchievement: "-0,5",
              desElement: "AS ROMA",
              idAchievement: "MDE3OQ==",
              idBet: "MDI=",
              idElement: "WDAxNDM=",
              idEvent: "Nzc2OTA=",
              officialRotation: "15980"
            },
            {
              achievementValue: "-140",
              conditionAchievement: "2,5",
              desElement: "ALTA",
              idAchievement: "MDE4MA==",
              idBet: "MDM=",
              idElement: "MDAwMDc=",
              idEvent: "Nzc2OTA=",
              officialRotation: "0"
            },
            {
              achievementValue: "+110",
              conditionAchievement: "2,5",
              desElement: "BAJA",
              idAchievement: "MDE4MQ==",
              idBet: "MDM=",
              idElement: "MDAwMDg=",
              idEvent: "Nzc2OTA=",
              officialRotation: "0"
            }
          ],
          lstTypeBetEvent: [
            {
              idBet: "MDE=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            },
            {
              idBet: "MDI=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            },
            {
              idBet: "MDM=",
              idEvent: "Nzc2OTA=",
              staBet: "1"
            }
          ],
          staEvent: "1"
        }
      ],
      lstLeague: [
        {
          desLeague: "EUROPA LEA",
          idLeague: "MjE5",
          idSport: "MDI="
        }
      ]
    },
    message: {
      code: "000",
      description: "Respuesta Exitosa."
    }
  };

  const deportes_ = [statictData.data.lstDSport];
  const ligas_ = statictData.data.lstLeague;
  const apuestas_ = statictData.data.lstDtBet;
  const eventosPorLigas = statictData.data.lstDtEvent.reduce((leagues, evento) => ({
    ...leagues,
    [evento.idLeague]: [
      ...(leagues[evento.idLeague] || []),
      {
        desEvent: evento.desEvent,
        idEvent: evento.idEvent,
        idLeague: evento.idLeague,
        idSport: evento.idSport,
        lstAchievement: evento.lstAchievement,
        dateEvent: evento.dateEvent,
      }
    ]
  }), {});
  const deportesObj_ = deportes_.reduce((sports, sport) => ({...sports, [sport.idSport]: sport}), {});
  const ligasObj_ = ligas_.reduce((leagues, league) => ({...leagues, [league.idLeague]: league}), {});

  export const configSport = statictData.data.configSport; 
  export const configuracion = statictData.data.configSport;
  export const deportes = deportes_;
  export const ligas = ligas_;
  /**
   * @returns { {desSport: string; idSport: string; staParlay: string;} }
   */
  export const deportesObj = (sportId) => deportesObj_[sportId];
  /**
   * @returns { {desLeague: string; idLeague: string; idSport: string;} }
   */
  export const ligasObj = (leagueId) => ligasObj_[leagueId];
  export const apuestasObj = apuestas_.reduce((bets, bet) => ({...bets, [bet.idBet]: bet}), {});
  export const tipoMovimiento = statictData.data.lstDtBMove;
  export const tipoApuesta = statictData.data.lstDtBet;
  export const apuestasDisponiblesPorDeporte = statictData.data.lstDtBetAllow.reduce((sportsAndBets, sportDta) => ({...sportsAndBets, [sportDta.idSport]: sportDta.lstBet}),{});
  /**
   * @returns { null | {desEvent: string; idEvent: string; idLeague: string; idSport: string; lstAchievement: {achievementValue: string; conditionAchievement: string; desElement: string; idAchievement: string; idBet: string; idElement: string; idEvent: string; officialRotation: string;}[]; dateEvent: string;}[] }
   */
  export const eventosPorLiga = (leagueId) => (eventosPorLigas[leagueId] || null);
  /**
   * @returns { {desSport: string; idSport: string; staParlay: string; icon: string; leagues: {desLeague: string; idLeague: string; idSport: string;}[];}[] } 
   */
  export const menu = () => {
    const sports = ligas_.reduce((sports, league) => ({
      ...sports, 
      [league.idSport]: {
        leagues: [
          ...(sports[league.idSport]?.leagues || []),
          league
        ],
      }
    }), {});

    deportes_.forEach((sport) => {
      if (sports[sport.idSport]) sports[sport.idSport] = {
        ...sports[sport.idSport],
        ...sport,
        icon: 'cicSoccer',
      }
    });

    return Object.keys(sports).map(sportId => sports[sportId])
  };
  export const wholeRawData = {...statictData};