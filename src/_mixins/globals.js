//Metodos:
// Characters min: 14, max: 160. Seconds min: 2.5, max: 8
function getMsgSeconds (str = '') {
  const len = str.length < 14 ? 14 : str.length > 160 ? 160 : str.length;
  const seconds = (( ( len * 55000 ) + 2880000 ) / 1460);
  return Math.round(seconds)
}

function fireToastMsg (msg, icon = "warning") {
    return this.$swal.fire({
      position: "top-end",
      toast: true,
      icon,
      title: msg || '',
      showConfirmButton: false,
      timer: this.getMsgSeconds(msg || ''),
      timerProgressBar: true,
    });
}

export default {
  methods: {
    getMsgSeconds,
    fireToastMsg,
  },
}