export const LABELS = {
    email: 'Debe tener un formato de correo correcto',
    mustHave: (min, max = 0) => `Debe tener ${!max ? `una longitud de ${min}` : (!min ? `máximo ${max}` : `entre ${min} a ${max}`)} caracteres`,
    numberAndDecimals: (max) => `Debe tener formato numérico y solo se admiten máximo ${max} decimales`,
    numeric: 'Debe ser solo números',
    onlyLettersSpecial: 'Debe ser solo letras.',
    passwordRgx: 'Debe tipear letras y/o números sin espacios en blanco',
    required: 'Campo requerido',
    sameAsPassword: 'Las claves no coinciden',
};