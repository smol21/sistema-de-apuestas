export const dateDepocher = (dateEpoch, slash = true) => {
    const date_ = new Date(dateEpoch * 1000);
    const day = `${date_.getDate()}`;
    const month = `${date_.getMonth() + 1}`;
    const year = date_.getFullYear();
    const separator = `${slash && '/' || '-'}`;
    const dateDepoched = `${day.length == 1 && '0' || ''}${day}` + separator + `${month.length == 1 && '0' || ''}${month}` + separator + year;

    return dateDepoched
}

export const dateTimeOfTheDay = (dateEpoch) => {
    const date_ = new Date(dateEpoch * 1000);
    const hour = `${date_.getHours()}`;
    const hourFormatted = +hour > 12 ? `${+hour - 12}` : hour;
    const minutes = `${date_.getMinutes()}`;
    const timeStr = `${hourFormatted.length == 1 && '0' || ''}${hourFormatted}:${minutes.length == 1 && '0' || ''}${minutes} ${+hour > 12 ? 'pm' : 'am'}`;

    return timeStr
}