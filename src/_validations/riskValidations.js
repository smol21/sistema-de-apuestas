import { cNumberAndDecimals, cRequired } from '@/_validations/validacionEspeciales';

export default {
    risk: { required: cRequired, numberAndDecimals: cNumberAndDecimals(2) },
    winTotal: { required: cRequired, numberAndDecimals: cNumberAndDecimals(2) }
}