/* eslint-disable */
import { LABELS as labels } from "@/_helpers/labels";
import { helpers } from "@vuelidate/validators";
import { required } from '@vuelidate/validators'

const cNumberAndDecimals = (max) => helpers.withMessage(
    ({ $params }) => labels.numberAndDecimals($params.max),
    helpers.withParams(
        { type: 'numberAndDecimals', max },
        (value) => /^\d{1,}(\.\d{1,2})?$/.test(value) //(value) => /^\d{1,}(\,\d{1,2})?$/.test(value)
    )
);

const cRequired = helpers.withMessage(labels.required, required);

export { 
    cNumberAndDecimals,
    cRequired
};