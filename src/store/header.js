import { defineStore } from 'pinia'

export const useHeader = defineStore('header', {
    state: () => {
        return {
            headerHeight: 64, // pixels
        }
    },
    actions: {
        updateHeaderHeight(payload) {
            this.headerHeight = payload
        },
    },
})