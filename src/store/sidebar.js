import { defineStore } from 'pinia'

export const useSidebar = defineStore('sidebar', {
    state: () => {
        return {
            sidebarVisible: true,
            sidebarUnfoldable: false
        }
    },
    getters: {
        isSidebarVisible: (state) => state.sidebarVisible,
    },
    actions: {
        toggleSidebar() {
            this.sidebarVisible = !this.sidebarVisible
        },
        toggleUnfoldable() {
            this.sidebarUnfoldable = !this.sidebarUnfoldable
        },
        updateSidebarVisible(payload) {
            this.sidebarVisible = payload
        },
    },
})