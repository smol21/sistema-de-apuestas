import { defineStore } from 'pinia'
import { apuestasObj, configSport, deportesObj, eventosPorLiga, ligasObj } from '@/views/staticData';

export const useBetStructure = defineStore('betstructure', {
    state: () => {
        return {
            nav: [],
            league: null,
            eventsByLeague: [],
            bets: null,
            config: null,
        }
    },
    // getters: {
    //     key: (state) => state.stateValue,
    // },
    actions: {
        setMenu(menu) {
            this.nav = menu
        },
        setLeague(leagueId) {
            this.config = configSport;

            const league = ligasObj(leagueId);
            const sport = league ? deportesObj(league.idSport) : null;
            
            this.league = league && sport ? {
                ...league,
                ...sport,
            } : null;

            const evsByL = eventosPorLiga(leagueId);
            this.eventsByLeague = evsByL && evsByL instanceof Array && evsByL.length ? evsByL : [];
            this.bets = apuestasObj;
        },
    },
})