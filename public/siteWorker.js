onmessage = function ({data}) {
  let consulting = false;

  const makeRequest = async () => {
    try {
      const res = await fetch('https://pokeapi.co/api/v2/pokemon/ditto');
      const data = await res.json();
      const result = JSON.stringify(data);
      consulting = false;
      postMessage(result);
    } catch (err) {
      console.error(err.message); 
      consulting = false;
    }
  };

  const shouldMakeRequest = () => {
    consulting = true;
    fetch('https://jsonplaceholder.typicode.com/todos/1')
      .then(response => response.json())
      .then(json => {
        const should = parseInt(Math.round(Math.random()));
        if (should) makeRequest();
        else consulting = false;
      })
      .catch(err => {
        console.error(err);
        consulting = false;
      })
  };

  if (!consulting) shouldMakeRequest();

  setInterval(() => {
    if (!consulting) shouldMakeRequest();
  }, 30000);
};